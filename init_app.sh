#!/bin/sh

cd ./src && composer install
docker-compose --file="../laradock/docker-compose.yml" --project-directory="../laradock/" up -d apache2 mysql

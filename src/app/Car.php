<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 * @OA\Property(property="id", type="string", example=1, readOnly="true"),
 * @OA\Property(property="created_at", type="string", format="date-time", description="Initial creation timestamp", readOnly="true"),
 * @OA\Property(property="updated_at", type="string", format="date-time", description="Last update timestamp", readOnly="true"),
 * @OA\Property(property="brand", type="string", example="Mitsubishi"),
 * @OA\Property(property="color", type="string", example="BlueViolet"),
 * @OA\Property(property="number", type="string", example="DWV-991"),
 * )
 * Class Car
 * @package App
 */
class Car extends Model
{
    protected $fillable = [
        'brand',
        'color',
        'number',
    ];

    public function drivers()
    {
        return $this->belongsToMany(Driver::class);
    }

    public function getCarsWithManyDrivers(): Collection
    {
        return $this
            ->has('drivers', '>', 1)
            ->has('drivers', '<', 4)
            ->get();
    }
}

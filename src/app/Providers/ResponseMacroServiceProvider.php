<?php

namespace App\Providers;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('notImplemented', function() {
            return Response::json([
                'status_code' => 400,
                'error' => 'Not implemented.'
            ]);
        });
    }
}

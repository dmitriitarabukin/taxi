<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;

class PrepareDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:prepare';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refreshes and seeds tables.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        Schema::disableForeignKeyConstraints();
        Artisan::call('migrate:refresh --path=/database/migrations/2020_11_01_161749_create_drivers_table.php');
        Artisan::call('migrate:refresh --path=/database/migrations/2020_11_01_174802_create_operators_table.php');
        Artisan::call('migrate:refresh --path=/database/migrations/2020_11_01_221238_create_cars_table.php');
        Artisan::call('migrate:refresh --path=/database/migrations/2020_11_01_225415_create_orders_table.php');
        Artisan::call('migrate:refresh --path=/database/migrations/2020_11_02_023758_create_car_driver_table.php');
        Schema::enableForeignKeyConstraints();

        Artisan::call('db:seed');

        $this->info('Preparing database completed successfully.');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * @OA\Schema(
 *   oneOf={
 *     @OA\Schema(ref="#/components/schemas/Driver"),
 *     @OA\Schema(ref="#/components/schemas/Operator"),
 *   }
 * )
 * Class Staff
 * @package App
 */
class Staff extends Model
{
    public function getAllStaff(): Collection
    {
        return DB::table('operators')
            ->union(DB::table('drivers'))
            ->get();
    }
}

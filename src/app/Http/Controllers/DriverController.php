<?php

namespace App\Http\Controllers;

use App\Driver;
use Illuminate\Http\Request;

class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->notImplemented();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return response()->notImplemented();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->notImplemented();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return response()->notImplemented();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->notImplemented();
    }

    /**
     * @OA\Get(
     * path="/api/drivers/no-orders",
     * summary="Найти всех водителей, которые не имеют ни одного заказа за всё время работы (минимум 2 в ответе)",
     * operationId="driversNoOrders",
     * tags={"drivers"},
     * security={{"sanctum":{}}},
     * @OA\Response(
     *    response=200,
     *    description="Success",
     *     @OA\JsonContent(
     *       @OA\Property(property="status", type="integer", example=200),
     *       @OA\Property(property="data", type="array",
     *       @OA\Items(ref="#/components/schemas/Driver")),
     *     ),
     * ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthenticated",
     *    @OA\JsonContent(
     *      @OA\Property(property="status", type="integer", example=401),
     *      @OA\Property(property="message", type="string", example="Unauthenticated.")
     *    ),
     * )
     * )
     * @param Driver $driver
     * @return \Illuminate\Http\JsonResponse
     */
    public function noOrders(Driver $driver)
    {
        return response()->json([
            'status' => 200,
            'data' => $driver->getDriversThatDontHaveOrders(),
        ]);
    }

    /**
     * @OA\Get(
     * path="/api/drivers/more-than-one-hundred-completed-orders",
     * summary="Найти всех водителей, которые имеют более 100 выполненных заказов ('Not Found')",
     * operationId="driversMoreThanOneHundredOrders",
     * tags={"drivers"},
     * security={{"sanctum":{}}},
     * @OA\Response(
     *    response=200,
     *    description="Success",
     *     @OA\JsonContent(
     *       @OA\Property(property="status", type="integer", example=200),
     *       @OA\Property(property="message", type="string", example="Not Found"),
     *     ),
     * ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthenticated",
     *    @OA\JsonContent(
     *      @OA\Property(property="status", type="integer", example=401),
     *      @OA\Property(property="message", type="string", example="Unauthenticated.")
     *    ),
     * )
     * )
     * @param Driver $driver
     * @return \Illuminate\Http\JsonResponse
     */
    public function moreThanOneHundredCompletedOrders(Driver $driver)
    {
        $drivers = $driver->getDriversWithMoreThanOneHundredCompletedOrders();

        if ($drivers->count() > 0) {
            return response()->json([
                'status' => 200,
                'data' => $drivers,
            ]);
        }

        return response()->json([
            'status' => 200,
            'message' => 'Not Found',
        ]);
    }

    /**
     * @OA\Get(
     * path="/api/drivers/more-than-ten-completed-orders",
     * summary="Найти всех водителей, которые имеют более 10 выполненных заказов (минимум 1 водитель)",
     * operationId="driversMoreThanTenHundredOrders",
     * tags={"drivers"},
     * security={{"sanctum":{}}},
     * @OA\Response(
     *    response=200,
     *    description="Success",
     *     @OA\JsonContent(
     *       @OA\Property(property="status", type="integer", example=200),
     *       @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/Driver")),
     *     ),
     * ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthenticated",
     *    @OA\JsonContent(
     *      @OA\Property(property="status", type="integer", example=401),
     *      @OA\Property(property="message", type="string", example="Unauthenticated.")
     *    ),
     * )
     * )
     * @param Driver $driver
     * @return \Illuminate\Http\JsonResponse
     */
    public function moreThanTenCompletedOrders(Driver $driver)
    {
        return response()->json([
            'status' => 200,
            'data' => $driver->getDriversWithMoreThanTenCompletedOrders(),
        ]);
    }

    /**
     * @OA\Get(
     * path="/api/drivers/by-orders-completed-desc",
     * summary="Вывести список водителей в порядке убывания количества выполненных заказов (имена всех водителей)",
     * operationId="driversByOrdersCompletedDesc",
     * tags={"drivers"},
     * security={{"sanctum":{}}},
     * @OA\Response(
     *    response=200,
     *    description="Success",
     *     @OA\JsonContent(
     *       @OA\Property(property="status", type="integer", example=200),
     *       @OA\Property(property="data", type="array", @OA\Items(type="string", example="Jaylon Mueller Sr.")),
     *     ),
     * ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthenticated",
     *    @OA\JsonContent(
     *      @OA\Property(property="status", type="integer", example=401),
     *      @OA\Property(property="message", type="string", example="Unauthenticated.")
     *    ),
     * )
     * )
     * @param Driver $driver
     * @return \Illuminate\Http\JsonResponse
     */
    public function byOrdersCompletedDesc(Driver $driver)
    {
        return response()->json([
            'status' => 200,
            'data' => $driver->getDriversByOrdersCompletedDesc(),
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Staff;

class StaffController extends Controller
{
    /**
     * @OA\Get(
     * path="/api/staff",
     * summary="Список всех сотрудников службы такси (водителей и операторов)",
     * operationId="allStaff",
     * tags={"staff"},
     * security={{"sanctum":{}}},
     * @OA\Response(
     *    response=200,
     *    description="Success",
     *     @OA\JsonContent(
     *       @OA\Property(property="status", type="integer", example=200),
     *       @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/Staff")),
     *     ),
     * ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthenticated",
     *    @OA\JsonContent(
     *      @OA\Property(property="status", type="integer", example=401),
     *      @OA\Property(property="message", type="string", example="Unauthenticated.")
     *    ),
     * )
     * )
     * @param Staff $staff
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Staff $staff)
    {
        return response()->json([
            'status' => 200,
            'data' => $staff->getAllStaff(),
        ]);
    }
}

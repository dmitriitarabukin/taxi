<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->notImplemented();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return response()->notImplemented();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->notImplemented();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return response()->notImplemented();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->notImplemented();
    }

    /**
     * @OA\Schema(
     *   schema="UncompletedOrders",
     *   allOf={
     *     @OA\Schema(@OA\Property(property="departure_address", ref="#/components/schemas/Order/properties/departure_address")),
     *     @OA\Schema(@OA\Property(property="destination_address", ref="#/components/schemas/Order/properties/destination_address")),
     *     @OA\Schema(@OA\Property(property="full_name", ref="#/components/schemas/Operator/properties/full_name")),
     *   }
     * )
     *
     * @OA\Get(
     * path="/api/orders/uncompleted",
     * summary="Найти все адреса, которые не имеют успешного статуса и имена операторов, которые их приняли (минимум 5 записей в ответе, операторы могут повторяться)",
     * operationId="uncompletedOrders",
     * tags={"orders"},
     * security={{"sanctum":{}}},
     * @OA\Response(
     *    response=200,
     *    description="Success",
     *     @OA\JsonContent(
     *       @OA\Property(property="status", type="integer", example=200),
     *       @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/UncompletedOrders")),
     *     ),
     * ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthenticated",
     *    @OA\JsonContent(
     *      @OA\Property(property="status", type="integer", example=401),
     *      @OA\Property(property="message", type="string", example="Unauthenticated.")
     *    ),
     * )
     * )
     * @param Order $order
     * @return \Illuminate\Http\JsonResponse
     */
    public function uncompleted(Order $order)
    {
        return response()->json([
            'status' => 200,
            'data' => $order->getAddressesAndOperatorsOfUncompletedOrders(),
        ]);
    }
}

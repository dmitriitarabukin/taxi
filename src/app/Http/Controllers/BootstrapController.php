<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Artisan;

class BootstrapController extends Controller
{
    /**
     * @OA\Post(
     * path="/api/init",
     * summary="Инициализация базы данных",
     * description="Удалить все таблицы из БД, если такие имеются и создать все необходимые таблицы, после чего внести в них тестовые данные: не менее 5 водителей, не менее 10 автомобилей, не менее 30 записей в истории заказов, не менее 3х заказов 'в процессе'",
     * operationId="bootstrapDB",
     * tags={"init"},
     * security={{"sanctum":{}}},
     * @OA\Response(
     *    response=200,
     *    description="Success",
     *     @OA\JsonContent(
     *       @OA\Property(property="status", type="integer", example=200),
     *       @OA\Property(property="message", type="string", example="Success."),
     *     ),
     * ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthenticated",
     *    @OA\JsonContent(
     *      @OA\Property(property="status", type="integer", example=401),
     *      @OA\Property(property="message", type="string", example="Unauthenticated.")
     *    ),
     * )
     * )
     */
    public function index()
    {
        Artisan::call('db:prepare');

        return response()->json([
            'status' => 200,
            'message' => 'Success.',
        ]);
    }
}

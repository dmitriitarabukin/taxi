<?php

namespace App\Http\Controllers;

use App\Car;
use Illuminate\Http\Request;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->notImplemented();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return response()->notImplemented();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->notImplemented();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return response()->notImplemented();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->notImplemented();
    }

    /**
     * @OA\Get(
     * path="/api/cars/with-many-drivers",
     * summary="Найти список автомобилей, на которых работают более 1го водителя и менее 4х водителей (минимум 2 варианта)",
     * operationId="carsWithManyDrivers",
     * tags={"cars"},
     * security={{"sanctum":{}}},
     * @OA\Response(
     *    response=200,
     *    description="Success",
     *     @OA\JsonContent(
     *       @OA\Property(property="status", type="integer", example=200),
     *       @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/Car")),
     *     ),
     * ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthenticated",
     *    @OA\JsonContent(
     *      @OA\Property(property="status", type="integer", example=401),
     *      @OA\Property(property="message", type="string", example="Unauthenticated.")
     *    ),
     * )
     * )
     * @param Car $car
     * @return \Illuminate\Http\JsonResponse
     */
    public function manyDrivers(Car $car)
    {
        return response()->json([
            'status' => 200,
            'data' => $car->getCarsWithManyDrivers(),
        ]);
    }
}

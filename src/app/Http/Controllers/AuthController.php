<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\User;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * @OA\Post(
     * path="/api/login",
     * summary="Аутентификация пользователя",
     * operationId="authLogin",
     * tags={"auth"},
     * @OA\RequestBody(
     *    required=true,
     *    description="Pass user credentials",
     *    @OA\JsonContent(
     *       required={"name","password"},
     *       @OA\Property(property="name", type="string", format="string", example="admin"),
     *       @OA\Property(property="password", type="string", format="password", example="123"),
     *    ),
     * ),
     * @OA\Response(
     *    response=401,
     *    description="Wrong credentials response",
     *    @OA\JsonContent(
     *       @OA\Property(property="status", type="integer", example=401),
     *       @OA\Property(property="error", type="string", example="Unable to login - invalid credentials."),
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Success",
     *     @OA\JsonContent(
     *       @OA\Property(property="status", type="integer", example=200),
     *       @OA\Property(property="access_token", type="string", example="1|eaNu8472NwvLQHAQP4ysl5cMnfOQzIyALaWhU58S"),
     *       @OA\Property(property="token_type", type="string", example="Bearer"),
     *     ),
     * )
     * )
     */
    public function login(LoginRequest $request)
    {
        $user = User::firstWhere('name', $request->name);

        if (\is_null($user) || (Hash::check($request->password, $user->password) === false)) {
            return response()->json([
                'status' => 401,
                'error' => 'Unable to login - invalid credentials.',
            ]);
        }

        return response()->json([
            'status' => 200,
            'access_token' => $user->createToken('auth_token')->plainTextToken,
            'token_type' => 'Bearer',
        ]);
    }
}

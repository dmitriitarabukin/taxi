<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 * @OA\Property(property="id", type="string", example=1, readOnly="true"),
 * @OA\Property(property="created_at", type="string", format="date-time", description="Initial creation timestamp", readOnly="true"),
 * @OA\Property(property="updated_at", type="string", format="date-time", description="Last update timestamp", readOnly="true"),
 * @OA\Property(property="full_name", type="string", example="Garret Block"),
 * @OA\Property(property="duty", type="string", example="Licensing Examiner and Inspector"),
 * )
 * Class Operator
 * @package App
 */
class Operator extends Model
{
    protected $guarded = [];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}

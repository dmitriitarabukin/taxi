<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * @OA\Schema(
 * @OA\Property(property="id", type="string", example=1, readOnly="true"),
 * @OA\Property(property="created_at", type="string", format="date-time", description="Initial creation timestamp", readOnly="true"),
 * @OA\Property(property="updated_at", type="string", format="date-time", description="Last update timestamp", readOnly="true"),
 * @OA\Property(property="departure_address", type="string", example="595 Nathaniel Pines\nBrakusport, IL 78314-7182"),
 * @OA\Property(property="destination_address", type="string", example="2577 Waelchi Junctions\nMarquardtchester, VT 49674"),
 * @OA\Property(property="completed", type="boolean", example=0),
 * @OA\Property(property="driver_id", type="integer", example=1, readOnly="true"),
 * @OA\Property(property="car_id", type="integer", example=1, readOnly="true"),
 * @OA\Property(property="operator_id", type="integer", example=1, readOnly="true"),
 * )
 * Class Order
 * @package App
 */
class Order extends Model
{
    protected $fillable = [
        'time',
        'departure_address',
        'destination_address',
        'completed',
        'driver_id',
        'car_id',
        'operator_id',
    ];

    public function car()
    {
        return $this->belongsTo(Car::class);
    }

    public function driver()
    {
        return $this->belongsTo(Driver::class);
    }

    public function operator()
    {
        return $this->belongsTo(Operator::class);
    }

    public function getAddressesAndOperatorsOfUncompletedOrders(): Collection
    {
        return DB::table('orders')
            ->join('operators','orders.operator_id','=','operators.id')
            ->select('departure_address', 'destination_address', 'operators.full_name')
            ->where('completed','=', false)
            ->get();
    }
}

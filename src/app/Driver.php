<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 * @OA\Property(property="id", type="string", example=1, readOnly="true"),
 * @OA\Property(property="created_at", type="string", format="date-time", description="Initial creation timestamp", readOnly="true"),
 * @OA\Property(property="updated_at", type="string", format="date-time", description="Last update timestamp", readOnly="true"),
 * @OA\Property(property="full_name", type="string", example="Asa Brekke"),
 * @OA\Property(property="duty", type="string", example="Credit Analyst"),
 * )
 * Class Driver
 * @package App
 */
class Driver extends Model
{
    protected $fillable = [
        'full_name',
        'duty',
    ];

    public function cars()
    {
        return $this->belongsToMany(Car::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function getDriversThatDontHaveOrders(): Collection
    {
        return $this
            ->doesntHave('orders')
            ->get();
    }

    public function getDriversWithMoreThanOneHundredCompletedOrders(): Collection
    {
        return $this->whereHas('orders', function(Builder $query) {
            $query->where('completed', true);
        }, '>', 100)->get();
    }

    public function getDriversWithMoreThanTenCompletedOrders(): Collection
    {
        return $this->whereHas('orders', function($query) {
            $query->where('completed', true);
        }, '>', 10)->get();
    }

    public function getDriversByOrdersCompletedDesc(): \Illuminate\Support\Collection
    {
        return $this->withCount(['orders as orders_completed' => function ($query) {
                $query->where('completed', true);
            }])
        ->orderBy('orders_completed', 'desc')
        ->pluck('full_name');
    }
}

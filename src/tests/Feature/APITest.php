<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class APITest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function database_successfully_bootstraps()
    {
        $this->login();

        $this->post('/api/init')
            ->assertStatus(200)
            ->assertJson([
                'status' => 200,
                'message' => 'Success.',
            ]);
    }

    /** @test */
    public function should_be_able_to_login_as_admin()
    {
        $this->login();

        $this->post('/api/login', ['name' => 'admin', 'password' => '123'])
            ->assertStatus(200)
            ->assertJson([
                'status' => 200,
                'token_type' => 'Bearer',
            ])
            ->assertJsonStructure([
                'status',
                'access_token',
                'token_type',
            ]);

        $this->assertAuthenticatedAs(User::firstWhere('name', 'admin'));
    }

    /** @test */
    public function request_should_fail_if_user_is_not_authenticated()
    {
        $this->get('/api/drivers/no-orders')
            ->assertStatus(200)
            ->assertJson([
                'status' => 401,
                'message' => 'Unauthenticated.',
            ]);
    }

    /** @test */
    public function should_return_at_least_two_drivers_without_orders()
    {
        $this->login();
        Artisan::call('db:prepare');

        $response = $this->get('/api/drivers/no-orders')
            ->assertStatus(200)
            ->assertJson(['status' => 200])
            ->assertJsonStructure([
                'status',
                'data' => [
                    '*' => [
                        'id',
                        'created_at',
                        'updated_at',
                        'full_name',
                        'duty',
                    ]
                ],
            ]);

        $this->assertGreaterThanOrEqual(2, \count($response->decodeResponseJson('data')));
    }

    /** @test */
    public function should_return_at_least_five_addresses_and_operator_names_with_uncompleted_orders()
    {
        $this->login();
        Artisan::call('db:prepare');

        $response = $this->get('/api/orders/uncompleted')
            ->assertStatus(200)
            ->assertJson(['status' => 200])
            ->assertJsonStructure([
                'status',
                'data' => [
                    '*' => [
                        'departure_address',
                        'destination_address',
                        'full_name',
                    ]
                ],
            ]);

        $this->assertGreaterThanOrEqual(5, \count($response->decodeResponseJson('data')));
    }

    /** @test */
    public function should_return_not_found_when_trying_to_find_drivers_that_have_more_than_one_hundred_completed_orders()
    {
        $this->login();
        Artisan::call('db:prepare');

        $this->get('/api/drivers/more-than-one-hundred-completed-orders')
            ->assertStatus(200)
            ->assertJson([
                'status' => 200,
                'message' => 'Not Found',
            ]);
    }

    /** @test */
    public function should_return_one_driver_that_have_completed_orders()
    {
        $this->login();
        Artisan::call('db:prepare');

        $response = $this->get('/api/drivers/more-than-ten-completed-orders')
            ->assertStatus(200)
            ->assertJson(['status' => 200])
            ->assertJsonStructure([
                'status',
                'data' => [
                    '*' => [
                        'id',
                        'created_at',
                        'updated_at',
                        'full_name',
                        'duty',
                    ]
                ],
            ]);

        $this->assertGreaterThanOrEqual(1, \count($response->decodeResponseJson('data')));
    }

    /** @test */
    public function should_return_a_list_of_drivers_names_in_descending_order_by_completed_orders()
    {
        $this->login();
        Artisan::call('db:prepare');

        $this->get('/api/drivers/by-orders-completed-desc')
            ->assertStatus(200)
            ->assertJson(['status' => 200])
            ->assertJsonStructure([
                'status',
                'data' => [
                    '*' => [
                    ]
                ],
            ]);
    }

    /** @test */
    public function should_return_two_cars_which_have_more_than_one_and_less_than_four_drivers()
    {
        $this->login();
        Artisan::call('db:prepare');

        $response = $this->get('/api/cars/with-many-drivers')
            ->assertStatus(200)
            ->assertJson(['status' => 200])
            ->assertJsonStructure([
                'status',
                'data' => [
                    '*' => [
                        'id',
                        'created_at',
                        'updated_at',
                        'brand',
                        'color',
                        'number',
                    ]
                ],
            ]);

        $this->assertGreaterThanOrEqual(2, \count($response->decodeResponseJson('data')));
    }

    /** @test */
    public function should_return_a_list_of_all_staff()
    {
        $this->login();
        Artisan::call('db:prepare');

        $this->get('/api/staff')
            ->assertStatus(200)
            ->assertJson(['status' => 200])
            ->assertJsonStructure([
                'status',
                'data' => [
                    '*' => [
                        'id',
                        'created_at',
                        'updated_at',
                        'full_name',
                        'duty',
                    ]
                ],
            ]);
    }
}

<?php

namespace Tests;

use App\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\Sanctum;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function login()
    {
        Sanctum::actingAs(factory(User::class)->create(['name' => 'admin', 'password' => Hash::make('123')]));
    }
}

<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'AuthController@login')->name('login');

Route::middleware(['auth:sanctum'])->group(function() {
    Route::post('/init', 'BootstrapController@index');

    Route::get('/drivers/no-orders', 'DriverController@noOrders');
    Route::get('/drivers/more-than-one-hundred-completed-orders', 'DriverController@moreThanOneHundredCompletedOrders');
    Route::get('/drivers/more-than-ten-completed-orders', 'DriverController@moreThanTenCompletedOrders');
    Route::get('/drivers/by-orders-completed-desc', 'DriverController@byOrdersCompletedDesc');
    Route::apiResource('/drivers', 'DriverController');

    Route::get('/orders/uncompleted', 'OrderController@uncompleted');
    Route::apiResource('/orders', 'OrderController');

    Route::get('/cars/with-many-drivers', 'CarController@manyDrivers');
    Route::apiResource('/cars', 'CarController');

    Route::get('/staff', 'StaffController@index');
});

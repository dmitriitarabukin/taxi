<?php

use App\Driver;
use App\Order;
use Illuminate\Database\Seeder;

class DriversWithMoreThanTenCompletedOrdersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Driver::class, 2)
            ->create()
            ->each(function(Driver $driver) {
                $driver
                    ->orders()
                    ->saveMany(factory(Order::class, 15)->make([
                        'driver_id' => $driver->id,
                        'completed' => true,
                    ]));
            });
    }
}

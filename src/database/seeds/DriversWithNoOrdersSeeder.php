<?php

use App\Driver;
use Illuminate\Database\Seeder;

class DriversWithNoOrdersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Driver::class, 2)->create();
    }
}

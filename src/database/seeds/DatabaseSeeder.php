<?php

use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            DriversWithNoOrdersSeeder::class,
            OrdersWithUnsuccessfulStatus::class,
            DriversWithMoreThanTenCompletedOrdersSeeder::class,
            CarSeeder::class,
        ]);
    }
}

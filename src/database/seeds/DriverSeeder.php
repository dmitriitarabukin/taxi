<?php

use App\Car;
use App\Driver;
use Illuminate\Database\Seeder;

class DriverSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Driver::class, 5)
            ->create()
            ->each(function(Driver $driver) {
                $driver->cars()->save(factory(Car::class, 3)->make());
            });
    }
}

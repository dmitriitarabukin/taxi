<?php

use App\Car;
use App\Driver;
use Illuminate\Database\Seeder;

class CarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cars = factory(Car::class, 10)->create();
        $drivers = factory(Driver::class, 5)->create();

        $cars->each(function(Car $car) use ($drivers) {
            $car->drivers()->attach($drivers->random(rand(1, 3))->pluck('id')->toArray());
        });
    }
}

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Car;
use App\Driver;
use Faker\Generator as Faker;
use Faker\Provider\Fakecar;

$factory->define(Car::class, function (Faker $faker) {

    $faker->addProvider(new Fakecar($faker));

    return [
        'brand' => $faker->vehicleBrand,
        'color' => $faker->colorName,
        'number' => $faker->vehicleRegistration,
    ];
});

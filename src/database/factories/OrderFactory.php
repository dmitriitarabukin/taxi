<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Car;
use App\Driver;
use App\Operator;
use App\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'time' => $faker->dateTime,
        'departure_address' => $faker->address,
        'destination_address' => $faker->address,
        'completed' => $faker->boolean,
        'driver_id' => function() {
            return factory(Driver::class)->create()->id;
        },
        'car_id' => function() {
            return factory(Car::class)->create()->id;
        },
        'operator_id' => function() {
            return factory(Operator::class)->create()->id;
        }
    ];
});
